/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

App Object
Author: Kitchen Prague 2015
*/

define([
    'jquery', 
    'underscore', 
    'backbone',
    'handlebars', 
    'cocktail',
    // 'fetchCache','
    'cookbook',
    'bootstrap'
], function ($, _, Backbone, Handlebars, Cocktail) {

    // enables mixins[] inside of the view declaration
    // ---------------------------------------------------------------
    Cocktail.patch(CookBook);

    var CookbookApp = CookBook.Application.extend({
        Views: {},
        Models: {},
        Collections: {},
        Mixins: {},
        Routers: {},
        apiUrl: function(model, time) {
            time = (time) ? ',' + time : ''; 
            return 'https://api.forecast.io/forecast/ff71906839708c6386105624cc60994d/' + model.get('latitude') + ',' + model.get('longitude') + time + '?units=si';
        },

        // get application view 
        getView : function(viewName, throwError) {
            var view = false;
                
            throwError = _.isUndefined(throwError) ? true : throwError;

            if (_.has(App.Views, viewName)) {
                view = App.Views[viewName];
            }

            if (_.isFunction(view)) {
                return view;
            }

            if (throwError) {
                throw new CookBook.Error('Requested view -'+viewName+'- is not registered with the App');
            }

            return view;
        },

        getModel : function(modelName, throwError) {
            var model = false;

            throwError = _.isUndefined(throwError) ? true : throwError;

            if (_.has(App.Models, modelName)) {
                model = App.Models[modelName];
            }

            if (_.isFunction(model)) {
                return model;
            }

            if (throwError) {
                throw new CookBook.Error('Requested model -'+modelName+'- is not registered with the App');
            }

            return model;
        }
    });

    var App = new CookbookApp();

    return App;
});