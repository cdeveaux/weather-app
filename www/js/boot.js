/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Boot File

Start the APP

Author: Kitchen Prague 2015
*/

define([
    'app',
    'router',

    // Models
    'models/entity',
    'models/page',
    'models/city',

    // Collections
    'collections/cities',

    // Views
    'views/main',
    'views/city.current',
    'views/city.daySelection',

    // ItemViews
    'views/items/city',

    // Layouts
    'views/layouts/page',
    'views/layouts/cities',
    'views/layouts/city',
    'views/layouts/city.add'

], function (App) {

    // Modernizr Tests
    // ----------------------------------------------------------------
    Modernizr.addTest('firefox', function () {
        return !!navigator.userAgent.match(/firefox/i);
    });
    Modernizr.addTest("isios", function() {
        return navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? !0 : !1;
    });
    Modernizr.addTest("retina", function() {
        return window.devicePixelRatio && window.devicePixelRatio > 1;
    });

    // App Router
    // ----------------------------------------------------------------
    // @todo set configurations / settings for router
    // ex. { index : 'en/home', defaultLocale: 'en' }
    App.Router = new App.MainRouter({
        index: 'en/home',
        defaultLocale: 'en'
    });

    App.locale = 'en';

    // App Main View
    // ----------------------------------------------------------------
    App.Main = new App.Views.Main();

    var t = setTimeout(function(){
        App.start();
    }, 0);
    
    // Start history when our application is ready
    App.on('start', function() {
        App.Main.render();
        App.Router.startHistory();
    });

    
    
});