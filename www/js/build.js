({
    baseUrl: '.',
    paths: {  
       // -------------------------------------------------
        // Folder Paths 
        // -------------------------------------------------
        'views' : 'views',
        'mixins' : 'mixins',
        'templates' : '../templates',
        'data' : 'data',
        // -------------------------------------------------

         // Require Extensions 
        // -------------------------------------------------
        'text':                     '../bower_components/requirejs-text/text',
        'json':                     '../bower_components/requirejs-plugins/src/json', 
        'async':                    '../bower_components/requirejs-plugins/src/async', 
        'noext':                    '../bower_components/requirejs-plugins/src/noext',


        // Libs
        // -------------------------------------------------

        'jquery':                   '../bower_components/jquery/dist/jquery.min',
        'underscore':               '../bower_components/underscore/underscore-min',
        'backbone':                 '../bower_components/backbone/backbone',
        'cocktail':                 '../bower_components/cocktail/Cocktail-0.5.10.min',
        'cookbook':                   '../bower_components/backbone.cookbook/backbone.cookbook',

        'viewOptions':              '../bower_components/backbone.viewOptions/backbone.viewOptions',
        'handlebars':               '../bower_components/handlebars/handlebars.min',
        'queryParams' :             '../bower_components/backbone-query-parameters/backbone.queryparams',

        'querystring' :             '../bower_components/querystring/querystring.min',
        'moment' :                  '../bower_components/moment/min/moment.min',

        // 'preloadJS' :               '../bower_components/PreloadJS/lib/preloadjs-0.6.0.combined',
        'imagesLoaded':             '../bower_components/imagesloaded/imagesloaded',
        'imgLiquid' :               '../bower_components/imgLiquid/js/imgLiquid-min',

        'modernizr':                '../bower_components/modernizr/modernizr',
        'smartresize':              '../bower_components/jquery-smartresize/jquery.debouncedresize',
        'bootstrap':                '../bower_components/sass-bootstrap/dist/js/bootstrap.min',
        'tweenmax':                 '../bower_components/gsap/src/minified/TweenMax.min',
        
        'fittext':                  '../bower_components/jquery-fittext.js/jquery.fittext',
        
        'SVGInjector':              '../bower_components/svg-injector/dist/svg-injector.min',

        'PxLoader':                 '../bower_components/PxLoader/dist/pxloader-images.min',

        'leaflet':                  '../bower_components/leaflet/dist/leaflet',
        'royalslider':              'libs/jquery.royalslider.min',
    },
    name: "load",
    preserveLicenseComments: false,

    optimize: "uglify2",
    // generateSourceMaps: true,
    
    out: "app.min.js"
})