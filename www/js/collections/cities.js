define([
    'app',
    'querystring',
    'models/city',
    'localstorage'
], function (App, Querystring) {

    App.Collections.Cities = CookBook.Collection.extend({
        localStorage: new Backbone.LocalStorage("cities"),
       	model : App.Models.City,

        // dynamic url
        url: function(){
            return '';
        },

        initialize: function(models, options){
			var self = this;
            self.options = options || {};
		},

		parse: function(resp, options){
            var data = resp.data;
            return data;
        }

    });

});