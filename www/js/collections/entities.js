/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Entities Collection

Author: Kitchen Prague 2015
*/

define([
    'app',
    'querystring',
    'models/entity'
], function (App, Querystring) {

    App.Collections.Entities = CookBook.Collection.extend({

       	model : App.Models.Entity,


        _defaultQueryParams : {
            'with' : 'assets,relations'
        },

        // dynamic url
        url: function(){
            var url = absurl + "/api/entity?",
                query = {};

            /**
             * Convert array to comma separated string.
             *
             * 1. { "id[in]" : [1,2] } -> id[in][]=1&id[in][]=2
             * 2. { "id[in]" : "1,2" } -> id[in]=1,2
             *
             * @note these should be both handled by the api.
             */
            if (this.options && this.options.query){
                query = _.mapObject(this.options.query, function(value, key) {
                    if (_.isArray(value)){ value = value.join(','); }
                    return value;
                });
            }

            query = _.defaults({}, query, _.result(this, '_defaultQueryParams'));

            url += Querystring.stringify(query);
            return url;
        },

        initialize: function(models, options){
			var self = this;
            self.options = options || {};
		},

		parse: function(resp, options){
            var data = resp.data;
            return data;
        },

        fetch: function(options) {
            var self = this;

            // save / merge options
            if (options) {
                var query = _.has(options, 'query') ? options.query : null;
                self.options.query = query;
            }

            // call parent fetch 
            return self._super(options);
        }

    });

});