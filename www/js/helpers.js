/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Handlebars Helpers
Author: Kitchen Prague 2015
*/

define([
    'app',
    'handlebars'
], function (App, Handlebars) {	
		    	            
    Handlebars.registerHelper('if_eq', function(context, options) {
        if (context == options.hash.compare) return options.fn(this);
        return options.inverse(this);
    });

    Handlebars.registerHelper('unless_eq', function(context, options) {
        if (context == options.hash.compare) return options.inverse(this);
        return options.fn(this);
    });

    Handlebars.registerHelper('times', function(n, block) {
        var accum = '';
        for (var i = 1; i <= n; ++i)
        accum += block.fn(i);
        return accum;
    });
    
    Handlebars.registerHelper('repeat', function(n, options) {
	    options = options || {};
	    var _data = {};
	    if (options._data) {
	      _data = Handlebars.createFrame(options._data);
	    }
	
	    var content = '';
	    var count = n - 1;
	    for (var i = 0; i <= count; i++) {
	      _data = {
	        index: digits.pad((i + 1), {auto: n})
	      };
	      content += options.fn(this, {data: _data});
	    }
	    return new Handlebars.SafeString(content);
    });
    
    Handlebars.registerHelper('include', function(templatename, options){ 
        var partial = Handlebars.partials[templatename];
        var additional_data = options.hash.id || {};
        var context = $.extend({}, this, additional_data);
        return new Handlebars.SafeString(partial(context));
    });
    
    Handlebars.registerHelper('plusone', function(context){
     	return ++context;
    });

    Handlebars.registerHelper('locale', function() {
        return App.locale
    });
    
    Handlebars.registerHelper('getProp', function(context, options){
      if (context) return context[options + '_' + App.lang];
    });   
    

    Handlebars.registerHelper('formatDate', function(context){
        return context && context.replace(/-/g, "—");
    });
    
    Handlebars.registerHelper('translation', function(context){
        return new Handlebars.SafeString(App.Translations[App.options.locale][context]);
    });

    Handlebars.registerHelper('var_dump', function(context) {
        console.log(context);
        return context;
    })
});