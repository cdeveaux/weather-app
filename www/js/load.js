require.config({
    urlArgs: "noCache=" + (new Date).getTime(),
    baseUrl: '/js/',
    waitSeconds: 120,
    paths: {

        // Dir Paths 
        // -------------------------------------------------
        'views' :                   'views',
        'mixins' :                  'mixins',
        'models' :                  'models',
        'collections' :             'collections',
        'templates' :               '../templates',
        'partials' :                '../templates/hbs/partials',

        // Require Extensions 
        // -------------------------------------------------
        'text':                     '../bower_components/requirejs-text/text',
        'json':                     '../bower_components/requirejs-plugins/src/json', 
        'async':                    '../bower_components/requirejs-plugins/src/async', 
        'noext':                    '../bower_components/requirejs-plugins/src/noext',

        // Libs
        // -------------------------------------------------
        'jquery':                   '../bower_components/jquery/dist/jquery.min',
        'underscore':               '../bower_components/underscore/underscore-min',
        'backbone':                 '../bower_components/backbone/backbone',
        'localstorage':             '../bower_components/backbone.localStorage/backbone.localStorage',
        'cocktail':                 '../bower_components/cocktail/Cocktail-0.5.10.min',
        'cookbook':                 '../bower_components/backbone.cookbook/backbone.cookbook.min',

        'viewOptions':              '../bower_components/backbone.viewOptions/backbone.viewOptions',
        'handlebars':               '../bower_components/handlebars/handlebars.min',
        'queryParams' :             '../bower_components/backbone-query-parameters/backbone.queryparams',

        'querystring' :             '../bower_components/querystring/querystring.min',
        'moment' :                  '../bower_components/moment/min/moment.min',

        'modernizr' :               'libs/modernizr.custom.min',

        'smartresize':              '../bower_components/jquery-smartresize/jquery.debouncedresize',
        'bootstrap':                'libs/bootstrap.min',
        'tweenmax':                 '../bower_components/gsap/src/minified/TweenMax.min',
    },
 
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'Bootstrap'
        },

        queryParams: {
             deps: ['backbone']
        },
        
        smartresize: {
            deps: ['jquery'],
            exports: 'Smartresize'
        },

        tweenmax: {
            deps: ['jquery']
        }
    }
});

require([
    'app',
    'modernizr',
    'scripts',
    'boot'
], function (App) {
    
});