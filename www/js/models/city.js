define([
    'app',
    'models/entity',
    'localstorage'
], function (App) {
            
    App.Models.City = App.Models.Entity.extend({

        geocodeUrl: 'http://maps.googleapis.com/maps/api/geocode/json?address=',

    	url: function() {
    		var self = this;

    		return App.apiUrl(self);
    	},

        urlNextDay: function() {
            var self = this,
                midnight = next2midnight()

            return App.apiUrl(self, midnight);
        },

        geoUrl: function(city) {
            var self = this;

            return 'http://maps.googleapis.com/maps/api/geocode/json?address=' + city;
        },

        parse: function(resp, options){
            var data,
                days = resp.daily.data,
                dates = [],
                roundT = Math.round(resp.currently.temperature);

            resp.days = [];
            resp.name = resp.name.capitalize();

            // Round the temperature
            resp.currently.temperatureLabel = roundT + '°C';

            // Init the array with the next 3 days (including the current)
            for (var i=0; i<3; i++) {
                var day = days[i];
                dates.push(dayFromTime(day.time));
                resp.days.push({
                    time: day.time,
                    label: dayFromTimestamp(day.time),
                    hours: []
                })
            }

            // Group the hours by day
            _.each(resp.hourly.data, function(data) {
                var time = data.time,
                    t = dayFromTime(time),
                    idHour = hourFromTime(time);
  
                data.hour = idHour;
                data.temperatureLabel = Math.round(data.temperature) + '°C';

                _.each(dates, function(date, i) {
                    if (_.isEqual(t, dates[i])) {
                        resp.days[i].hours.push(data);
                    }
                });
            });

            data = this._super(resp, options);
            return data;
        },

        initialize: function() {
            var self = this;
            self._super();
        },

        create: function(name) {
            var self = this;
            

            return self.getCoordinates(name).then(function(data) {
                if (data.status == 'ZERO_RESULTS') {
                    return {
                        error: 'The city is not valid.'
                    }
                }
                
                var coordinates = data.results[0].geometry.location;
                // Assume the first city returned, was the one
                App.Cities.add(self);
                self.set({
                    name: name,
                    latitude: coordinates.lat,
                    longitude: coordinates.lng,
                    updated: 0
                });
                self.fetch({reset: true}, true);
                    
            }).promise();
        },

        getCoordinates: function(name) {
            var self = this;

            return $.ajax({
                url: self.geoUrl(name)
            }).promise();
        },

        // Override the localstorage fetch
        fetch: function(options, bust) {
            var self = this,
                bustCache = bust || (self.get('updated') && timeSecond() - self.get('updated') > 600);

            // Make a call to the API
            if (bustCache) {
                var currentDays = $.ajax({url: self.url()}),
                    day3 = $.ajax({url: self.urlNextDay()});

                // The API return only 48h of forecast, make another call to get the data to midnight on the last day
                return $.when( currentDays, day3 ).done(function ( firstDays, lastDay ) {
                    // Merge the missing forecast from the last day into the first days
                    var times = _.pluck(firstDays[0].hourly.data, 'time');
                    
                    _.each(lastDay[0].hourly.data, function(hour) {
                        if (times.indexOf(hour.time) > -1) return;
                        firstDays[0].hourly.data = firstDays[0].hourly.data.concat([hour]);
                    })
                    self.save(_.extend({},
                        self.attributes,
                        firstDays[0],
                        {updated: timeSecond()}
                    ))
                    if (options.reset) {
                        self.trigger('reset', self);
                    }
                }).promise();
            } else {
                return Backbone.CookBook.Model.prototype.fetch.call(this, options).promise();
            }
        }
    });

});