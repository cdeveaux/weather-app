/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Entity Model

This is the elementary model, all other models are extending this one

Author: Kitchen Prague 2015
*/

define([
    'app',
    'underscore',
    'querystring',
    'moment'
], function (App, _, Querystring, Moment) {
            
    App.Models.Entity = CookBook.Model.extend({
    
        initialize: function(attributes, options){
            var self = this;
            self.options = options || {};
            
        },

        _defaultQueryParams : {
            'limit' : 1
        },

        // dynamic url
        url: function(){
            return 'a';
        },

        fetch: function(options) {
            var self = this;
            
            // save / merge options
            if (options) {
                var query = _.has(options, 'query') ? options.query : null;
                self.options.query = query;
            }

            // call parent fetch 
            return self._super(options);
        },

        parse: function(resp, options){
            var self = this,
                data = _.has(resp, 'data') ? resp.data : resp;
            
            // parse children
            // @todo add parseChildren flag to options to make it optinal and extendable
            if ( self._parseChildren ) data = self._parseChildren(data, options) || {};

            // create JS Date obj
            data.date = Moment(data.created_at).format("MMM Do YYYY");
            
            return data;
        },

        _parseChildren: function(attributes, options){
            var self = this;

            if ( _.has(attributes, 'children') && ! _.isEmpty(attributes.children) ){
                _.each(attributes.children, function(value, key){
                    attributes.children[key] = new Backbone.Collection(value, {
                        model: App.Models.Entity
                    });
                });
            }

            return attributes;
        },
        
        getAttribute: function(key){
            var attributes = this.get("attributes") || {};
            return ( _.has(attributes, key) ) ? attributes[key].value : null;
        },

        getLocale : function(){
            var locale = this.get('locale');
            return _.has(locale, 'code') ? locale.code : null;
        },

        /**
         * Merges entity attributes to 
         * 
         * [ 'name', 'age' ]
         * 
         * @param  {object} data  
         * @param  {array} keys 
         */
        mergeAttributes : function(data, keys){
            var self = this;
            if ( !data || !_.has(data, 'attributes') ) { return; }
            var attrs = _.pick(data.attributes, keys);
            _.each(attrs, function(attr, key){
                self.set(key, attr.value);
            });
        }

        // getAsset     
        // getAssets
        // getRelation
        // getRelations
    });

});