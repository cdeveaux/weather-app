define([
    'app',
    'models/entity'
], function (App) {
            
    App.Models.Page = App.Models.Entity.extend({

        parse: function(resp, options){
            var data = this._super(resp, options);
            return data;
        },

        getTemplate: function(){
            var tmpl = this.get('template');
            
            if (!tmpl) {
                // throw new CookBook.Error('View Template not defined.');
            }
            tmpl = tmpl.capitalize();
            return tmpl;
        }
    });

});