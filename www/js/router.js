define([
    'app',
], function(App) {

    App.MainRouter = CookBook.AppRouter.extend({

        routes: {
            ""                  : "showAll",
            "cities/add"        : "add",
            "cities/:city"      : "show",
        },

        initialize : function(options) {
            var self = this,
                models = [];

            self.pageModel = new App.Models.Page({
                template: 'cities'
            });

            App.Cities = new App.Collections.Cities();
            App.Cities.fetch().done(function(data) {
                App.Cities.reset(data);
            });
        },

        showAll: function() {
            var self = this,
                nbFetch = 0;
            
            App.template = 'Cities';

            // LocalStorage is empty, render an empty view
            if (!App.Cities.models.length) {
                self.render(Backbone.history.fragment, null, App.Cities);
                return;
            }

            // else fetch the cities
            _.each(App.Cities.models, function(city) {
                city.fetch({parse: false}).done(function(data) {
                    nbFetch++;
                    if (nbFetch < App.Cities.models.length) return;
                    self.render(Backbone.history.fragment, null, App.Cities);
                });
            });
        },
        
        show: function(city) {
            var self = this,
                model;

            App.template = 'City';
            model = _.find(App.Cities.models, function(m) {return m.get('name') == city});
            model.fetch().done(function() {
                self.render(Backbone.history.fragment, null, model);
            });
        },
        
        add: function() {
            var self = this;

            App.template = 'CityAdd';
            self.render(Backbone.history.fragment, null);
        },

        /**
         * Proccess the requested url
         * 
         * @param  array routeArgs 
         * @return string
         */
        proccessRequestedUrl: function(routeArgs) {

            var url = routeArgs ? routeArgs[0] : Backbone.history.getFragment(),
                locale = this.getOption('defaultLocale');

            if (_.isNull(url) || !url.length) {
                url = this.getOption('index');
            }

            var urlSegments = url.split( '/' ),
                firstSegment = urlSegments[0],
                localeRegExp = new RegExp(/^[a-zA-Z]{2}$/);

            if (localeRegExp.test(firstSegment)) {
                locale = firstSegment;
            }

            App.locale = locale;
            return url;
        },
              
        render: function(url, params, pageData){
            App.Main.changePage(pageData);
        },

        back: function() {
            alert('a');
        }
    });

});