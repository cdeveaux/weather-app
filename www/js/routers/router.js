/*
Navigator Subrouter
*/

define(['app'], function(App) {

    App.Routers.Cities = CookBook.AppRouter.extend({
        routes: {
            "cities"            : "showAll",
            "cities/:city"      : "show"
            "cities/add"        : "add",
        },
        
        initialize: function() {
            this.firstInit = true;
        },

        getCities: function(lang, callback) {
            var self = this,
                pageModel = new App.Models.Page();

            // First time, fetch the data
            if (!self.firstInit) {
                callback();
            }

            // TODO: Get the cities from the database
            // Get the hardcoded city (Prague)
            self.cities = new App.Collections.Cities();

            self.listenTo(self.cities, 'reset', function() {
                self.render(url, null, pageModel);
                callback();
            });

            self.cities.reset([new App.Models.City({
                name: 'Prague'
            })]);
        },

        showAll: function(params) {
            var self = this;
            self.getCities(function() {
                App.trigger("cities:show");
            })
        },
        
        show: function(lang, params) {
            var self = this;
            self.getCities(function() {
                App.trigger("cities:showAll");
            })
        },
        
        add: function(lang, params) {
            var self = this;
            self.getCities(function() {
                App.trigger("cities:add");
            })
        },

        // fired before every route. 
        execute: function(callback, args, name) {
            /**
             * HACK
             * 
             * when first initialized from app router 
             * execute is fired in both the app router and here
             * making the route history incorrect
             * 
             * @todo find a way aroud this
             */
            if ( this.notFirst ) App.Router.execute(null, args, name);
            this.notFirst = true;

            if (callback) callback.apply(this, args);
        }

    });

});