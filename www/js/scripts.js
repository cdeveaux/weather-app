/*********************************
 * Usefull Scipts 
 * Author: Various
*********************************/

// Return the distance between 2 coordinates (used in $.distanceFromMouse)
function getDistance(x,y,x2,y2) {
    return Math.floor(Math.sqrt(Math.pow(x - x2, 2) + Math.pow(y - y2, 2)));
};

function getDocumentWidth(){
    return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
}

function getDocumentHeight(){
    return Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
}

function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}

function getScrollTop() {
    if (typeof pageYOffset != 'undefined') {
        //most browsers
        return pageYOffset;
    } else {
        var B = document.body; //IE 'quirks'
        var D = document.documentElement; //IE with doctype
        D = (D.clientHeight) ? D : B;
        return D.scrollTop;
    }
}


/*********************************
 * Timestamp / Date helpers
*********************************/

// Return the timestamp in seconds
function timeSecond() {
    return Math.floor(Date.now() / 1000);
}

// Return the timestamp at 00:00:00 in 2 days (If called on tuesday, will return Friday 00:00:00)
function next2midnight() {
    var date = new Date();
    date.setHours(48,0,0,0);
    return date.toAPIString();
}

// Return a date object, given a timestamp
function dateFromTime(time) {
    return new Date(time * 1000);
}

// Return a date object at midnight, given a timestamp
function dayFromTime(time) {
    var date = dateFromTime(time);
    date.setHours(0,0,0,0);
    return date;
}

// Return the hour, given a timestamp
function hourFromTime(time) {
    var date = dateFromTime(time);
    return date.getHours();
}

function currentHour() {
    var date = new Date();
    return date.getHours();
}

// Return the day, given a timestamp
function dayFromTimestamp(time) {
    var today = new Date().getDay(),
        day = dateFromTime(time).getDay();

    if (day == today) return 'Tod';
    switch(day) {
        case 0:
            return 'Sun';
        case 1:
            return 'Mon';
        case 2:
            return 'Tue';
        case 3:
            return 'Wed';
        case 4:
            return 'Thu';
        case 5:
            return 'Fri';
        case 6:
            return 'Sat';
    }
}

// Return the date in ISO 8601 for the API
Date.prototype.toAPIString = function() {
    return this.toISOString().replace('.','-').replace('Z',0);
}

function getTempRange(temp) {
    var self = this,
        temperatures = [-40,-37,-34,-31,-28,-25,-22,-19,-16,-13,-10,-7,-4,-1,0,3,6,9,12,15,18,21,24,27,30,33,36,39,40],
        classeCss = ['-40','-37-39','-34-36','-31-33','-28-30','-25-27','-22-24','-19-21','-16-18','-13-15','-10-12','-7-9','-4-6','-1-3','0','1-3','4-6','7-9','10-12','13-15','16-18','19-21','22-24','25-27','28-30','31-33','34-36','37-39','40'];

    return classeCss[Math.min(_.sortedIndex(temperatures, temp), classeCss.length-1)];
}

//////////////
// MATH UTILS
//////////////

function randomRange(min, max) {
    return min + (max - min) * Math.random();
}

function clamp(x, min, max) {
    return x < min ? min : (x > max ? max : x);
}

function sign(x) {
    return x < 0 ? -1 : 1;
}

// Check if its IE
function isIE() { 
    return ((navigator.appName == 'Microsoft Internet Explorer') || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null))); 
}

// check if Internet Explorer 11
var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);

// check if its IPAD
var isiPad = navigator.userAgent.match(/iPad/i) != null;


define(['jquery'], function($) {

    if (navigator.userAgent.match(/iPad;.*CPU.*OS 7_\d/i)) {
        $('html').addClass('ipad ios7');
    }
    
    // Returns parsed URL query params as an object
    $.extend({
      getQueryParameters : function(str) {
          return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
      }
    });

    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    
    if (!Object.keys) {
      Object.keys = function(obj) {
        var keys = [];
    
        for (var i in obj) {
          if (obj.hasOwnProperty(i)) {
            keys.push(i);
          }
        }
    
        return keys;
      };
    }
    
    
    $.fn.getRotationDegrees = function(){
            var obj = this;
            var matrix = obj.css("-webkit-transform") ||
            obj.css("-moz-transform")    ||
            obj.css("-ms-transform")     ||
            obj.css("-o-transform")      ||
            obj.css("transform");
            if(matrix !== 'none') {
                var values = matrix.split('(')[1].split(')')[0].split(',');
                var a = values[0];
                var b = values[1];
                var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
            } else { var angle = 0; }
            return (angle < 0) ? angle +=360 : angle;
    }

    $.fn.reverse = function() {
        return $(this.get().reverse());
    }

    
    // Cross Browser get Document Height
    $.fn.getDocHeight = function() {
        var D = this[0];
        var height = Math.max(
            Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
            Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
            Math.max(D.body.clientHeight, D.documentElement.clientHeight)
        );
        return (navigator.userAgent.match(/iPad/i) != null) ? height  : height;
        return height;
    }

    // return the difference on x and y between the element and the object passed in parameter
    $.fn.distanceFromEl = function(el) {
        var x = el.x,
            y = el.y,
            offset = this.offset(),
            left = offset.left,
            top = offset.top,
            w = this.width(),
            h = this.height(),
            right = offset.left + w,
            bottom = offset.top + h,
            result;

        // On the left of the element
        if (x <= left) {
            result = switchY(
                function() {return {x: left - x, y: top - y}},
                function() {return {x: left - x, y: 0}},
                function() {return {x: left - x, y: bottom - y}}
            );
        // On the right of the element  
        } else if (x > left && x < right) {
            result = switchY(
                function() {return {x: 0, y: top - y}},
                function() {return {x: 0, y: 0}},
                function() {return {x: 0, y: bottom - y}}
            );
        } else if (x >= right) {
            result = switchY(
                function() {return {x: right - x, y: top - y}},
                function() {return {x: right - x, y: 0}},
                function() {return {x: right - x, y: bottom - y}}
            );
        }

        function switchY(above, inside, beneath) {
            var res;
            
            if (y <= top) {
                res = above();
            } else if (y > top && y < bottom ) {
                res = inside();
            } else {
                res = beneath();
            }
         
            return res;
        }

        return {
            x: Math.abs(result.x),
            y: Math.abs(result.y)
        };
    }

    //Chunk array
    Array.prototype.chunk = function(chunkSize) {
        var array = this;
        return [].concat.apply([],
        array.map(function(elem, i) {
            return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
        }));
    }
    
    //get last array member
    Array.prototype.last = function() {
        return this[this.length-1]
    }

    //Swap elements in array
    Array.prototype.swapItems = function(a, b) {
        this[a] = this.splice(b, 1, this[a])[0];
        return this;
    }

    //Clone array
    Array.prototype.clone = function() {
        return this.slice(0);
    };

    // capitalize first letter
    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    // Using this for Tilt3d
    Array.prototype.mapRange = function(to) {
       var res = [],
           min = _.min(this),
           max = _.max(this);
       for (var i = 0; i < this.length; i++) {
           res[i] = to[0] + (this[i] - min) * (to[1] - to[0]) / (max - min);
       }
       return res;
    }
    
    
    // Format Money
    Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
        c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "." : t, 
        s = n < 0 ? "-" : "", 
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    // Checking if the elment is on screen
    $.fn.isOnScreen = function() {
        var win = $(window);
        var viewport = {
            top: win.scrollTop(),
            left: win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();
        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth() ;
        bounds.bottom = bounds.top + this.outerHeight() + 300;
        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };
    

    /**
     * jQuery.browser.mobile (http://detectmobilebrowser.com/)
     *
     * jQuery.browser.mobile will be true if the browser is a mobile device
     *
     **/ 
    (function(a) {
        (jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
    })(navigator.userAgent || navigator.vendor || window.opera);

    // make it safe to use console.log always
    (function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
    (function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());



    /**
     * Request Animation Frame Polyfill.
     * @author Tino Zijdel
     * @author Paul Irish
     * @see https://gist.github.com/paulirish/1579671
     */;
    (function() {

        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];

        for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame) {
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() {
                    callback(currTime + timeToCall);
                },
                timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };
        }

        if (!window.cancelAnimationFrame) {
            window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
        }

    }());
    
    // shuffle DOM elements
    $.fn.shuffle = function() {
 
        var allElems = this.get(),
            getRandom = function(max) {
                return Math.floor(Math.random() * max);
            },
            shuffled = $.map(allElems, function(){
                var random = getRandom(allElems.length),
                    randEl = $(allElems[random]).clone(true)[0];
                allElems.splice(random, 1);
                return randEl;
           });
 
        this.each(function(i){
            $(this).replaceWith($(shuffled[i]));
        });
 
        return $(shuffled);
 
    };
    
    /*
     * Konami Code For jQuery Plugin
     * 1.2.1, 23 October 2013
     *
     * Using the Konami code, easily configure and Easter Egg for your page or any element on the page.
     *
     * Copyright 2011 - 2013 Tom McFarlin, http://tommcfarlin.com
     * Released under the MIT License
     */
    (function(e) {
        "use strict";
        e.fn.konami = function(t) {
            var n, r, i, s;
            n = e.extend({}, e.fn.konami.defaults, t);
            return this.each(function() {
                i = [];
                e(window).keyup(function(e) {
                    s = e.keyCode || e.which;
                    if (n.code.length > i.push(s)) {
                        return
                    }
                    if (n.code.length < i.length) {
                        i.shift()
                    }
                    if (n.code.toString() !== i.toString()) {
                        return
                    }
                    n.cheat()
                })
            })
        };
        e.fn.konami.defaults = {
            code: [38, 38, 40, 40, 37, 39, 37, 39, 66, 65],
            cheat: null
        }
    })(jQuery)
    
    // PARALLAX
    //***************************************************

    $.fn.translateY = function(y) {
        var $this = $(this);
        if (Modernizr.csstransforms) {

            // use 3d transform if available for better performance
            var transform = (Modernizr.csstransforms3d) ? 'translate3d(0, ' + y + 'px, 0)' 
                                                        :  'translateY(' + y + 'px)';
            $this.css({
                '-webkit-transform': transform,
                '-moz-transform': transform,
                '-o-transform': transform,
                '-ms-transform': transform,
                'transform': transform
            });
        } else{
            $this.css({
                top: y + 'px'
            });
        }
    }

    // returning the scroll progress from 0 to 1 in between start/end pixels
    var scrollProgress = function(start, end){
        var scrollTop = getScrollTop(),
            factor = (end-start)/100,
            scrollStart = (scrollTop - start)/factor,
            percentage = (scrollTop >= start && scrollTop <= end ) ? scrollStart/100
                        : (scrollTop > start) ? 1
                        : 0;

        return percentage;
    }
    
    // translate element from start to end for a move
    $.fn.parallax = function(start, end, move){
        var $el = $(this),
            parallaxFactor = $el.data("factor") || 1,
            percentage = scrollProgress(start, end),
            y = percentage * move * parallaxFactor;
            
        $el.translateY(y); 
    }

    // END PARALLAX
    // *************************************************


}); 