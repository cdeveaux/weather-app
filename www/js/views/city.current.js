define([
	'app',
	'handlebars',
	'text!templates/hbs/city.current.hbs',
], function(App, Handlebars, Template) {

	App.Views.CityCurrent = CookBook.View.extend({

		template: Handlebars.compile(Template),

		events: {
		},

		mixins: [],

		initialize: function(options) {
			var self = this;

			self.currentDay = JSON.parse(JSON.stringify(options.currentDay));
			self.hour = _.find(self.currentDay.hours, {hour: options.hour});
			self.name = options.name;
			self.hour.rainLabel = Math.round(self.hour.precipProbability) + ' %';

			self.listenTo(App, 'hour:change', function(hour) {
				self.hour = _.find(self.currentDay.hours, {hour: hour});
				self.hour.rainLabel = Math.round(self.hour.precipProbability * 100) + ' %';
				self.render();
			});

		},

		serializeData: function() {
			var self = this
			return {
				hour: self.hour,
				name: self.name
			}
		}
	});		
	
});