define([
	'app',
	'handlebars',
	'text!templates/hbs/city.daySelection.hbs',
], function(App, Handlebars, Template) {

	App.Views.CityDaySelection = CookBook.View.extend({

		template: Handlebars.compile(Template),

		events: {
			'click .day__select' : 'updateDay'
		},

		mixins: [],

		initialize: function(options) {
			var self = this;

			_.bind(self.updateDay);

			self.days = JSON.parse(JSON.stringify(self.model.get('days')));
			
			// Set the current hour for each days
			_.each(self.days, function(day) {
				day.current = _.find(day.hours, {hour: options.hour}) || day.hours[0];
				day.range = 'js-temp-' + getTempRange(day.current.temperature);
			});

			self.listenTo(App, 'hour:change', function(hour) {
				
				_.each(self.days, function(day) {
					// First day, set the first hour as the current if we're out of range
					day.current = _.find(day.hours, {hour: hour});
					if (!day.current) day.current = day.hours[0];
					day.range = 'js-temp-' + getTempRange(day.current.temperature);
				});
				self.render();
			});
		},

		updateDay: function(e) {
			e.preventDefault();
			var self = this,
				$target = $(e.currentTarget),
				timestamp = $target.data('timestamp');

			App.trigger('day:change', timestamp);
		},

		serializeData: function() {
			var self = this;

			return {
				days: self.days
			}
		}

	});		
	
});