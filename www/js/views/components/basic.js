/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Basic Component View

All other components are extending this one

Author: Kitchen Prague 2015
*/

define([
	'app',
], function(App) {

	App.Views.BasicComponent = CookBook.View.extend({

		initialize: function(){
			var self = this;

			App.on('page:setSizes', function(){
            	if(_.isFunction(self.setSizes)) { 
            		setTimeout(function(){
            			self.setSizes();	
            		}); 
            	}
            });
		},

	    onRender: function(){
	    	var self = this;
	    	
	    	App.trigger("component:rendered");
	    },

	    setSizes: function(){
	    	var self = this;

	    	self.$el.removeAttr("style");
	    	var sizes = self.el.getBoundingClientRect();
	    	self.width = sizes.width;
	    	self.height = Math.floor(sizes.height);
	    }

	});		
	
});