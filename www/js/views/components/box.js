define([
	'app',
	'handlebars',
	'text!templates/hbs/components/box.hbs',
	'views/components/basic',
], function(App, Handlebars, Template) {

	App.Views.BoxComponent = App.Views.BasicComponent.extend({

		className: function(){
			return this.model.getClassName();	
		},

		template: Handlebars.compile(Template),

		mixins: [],

	    onRender: function(){
	    	var self = this;

	    	self._super();
	    },

	    setSizes: function(){
	    	var self = this;

	    	self._super();
	    	
	    	self.height = Math.floor(self.width * 0.6);

	    	self.$el.css({
	    		// width: self.width,
	    		height: self.height
	    	});
    	}

	});		
	
});