/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Footer View

Author: Kitchen Prague 2015
*/

define([
	'app',
	'handlebars',
	'text!templates/hbs/footer.hbs',
], function(App, Handlebars, Template) {

	App.Views.Footer = CookBook.View.extend({

		template: Handlebars.compile(Template),
		className: 'container fullHeight halfPaddingTB',
		id: 'footerContainer',

		mixins: [
			// App.Mixins.Fittext
		]

	});		
	
});