/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Header Component

This is the component being used to represend the featured product of the collection
Its appearing on collection layout

Author: Kitchen Prague 2015
*/

define([
	'app',
	'handlebars',
	'text!templates/hbs/components/header.hbs',
	'views/components/basic',
], function(App, Handlebars, Template) {

	App.Views.HeaderComponent = App.Views.BasicComponent.extend({

		events: {
			'click .explore' : 'scrollDown'
		},

		className: function(){
			return  this.model.className()
		},

		template: Handlebars.compile(Template),

		mixins: [],

		initialize: function(){
			var self = this;
			self._super();

			self.listenTo(App, 'screenshot:ready', function(){
                 // self.$(".placeHolder").remove();
            });
		},

		scrollDown: function(){
			var self = this;

			TweenMax.to($("body, html"), 0.8, {
                scrollTop: self.height,
                ease: 'Quad.easeOut'
            });
		},

		onBeforeRender: function(){
	    },

	    onRender: function(){
	    	var self = this;

	    	self._super();
	    },

	    onBeforeDestroy: function(){
        },

	    setSizes: function(){
	    	var self = this;

	    	self._super();

	    	self.height = App.height - App.topPadding;

	    	self.$el.css({
	    		height: App.height - App.topPadding
	    	});
	    	
	    	var $bborder = self.$(".bottomBorder"),
	    		$tborder = $(".topBorder"),
	    		$tborderLeft = $(".topBorderPart1"),
	    		$tborderRight = $(".topBorderPart2"),
	    		$content = self.$(".center"),
	    		$title = self.$(".title span"),
	    		offset = $content.offset();

	    	var topOffset =  offset.top - App.topPadding,
	    		leftOffset = offset.left - App.sidePadding,
	    		height = $content.height(),
	    		width = $content.width() * 0.6,
	    		titleHeight = $title.height(),
	    		titleWidth = $title.width();
	    	
	    	// Set the border bottom behind the product, but just under the text content
	    	// $bborder.css({
	    	// 	position: "absolute",
	    	// 	top: topOffset + height,
	    	// 	width: width,
	    	// 	left: leftOffset * 1.6
	    	// });

	    	// // Set the top borders on the left and right side of the title
	    	// $tborder.css({
	    	// 	position: "absolute",
	    	// 	top: topOffset + titleHeight/2,
	    	// 	width: (width - titleWidth) / 2 - (App.width * 0.01)
	    	// });

	    	// $tborderLeft.css({
	    	// 	left: leftOffset * 1.6
	    	// });

	    	// $tborderRight.css({
	    	// 	right: leftOffset * 1.6
	    	// });

    	}

	});		
	
});