/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Navigation View
Author: Kitchen Prague 2015
*/

define([
	'app',
	'handlebars',
	'text!templates/hbs/navigation.hbs',
], function(App, Handlebars, Template) {

	App.Views.Navigation = CookBook.View.extend({

		template: Handlebars.compile(Template),

		className: 'fullHeight navHolder',

		events: {
			'click .toggleServices' : 'toggleServices'
		},

		mixins: [],

		toggleServices: function(e) {
			var self = this;
			e && e.preventDefault();

			self.$(".navList").toggle();
		},

		onRender: function(){
		},

		initialize: function(){
			var self = this;

			App.on("menu:toggle", function(state){
				//open menu
				if(state){
					TweenMax.to($(".page"), 0.3, {
						opacity: 0,
						onComplete: function(){
							$(".page").hide();
						}
					});

					self.$el.show();
					TweenMax.to(self.$el, 0.3, {
						opacity: 1,
					});

				//close menu
				}else{
					$(".page").show();
					TweenMax.to($(".page"), 0.3, {
						opacity: 1
					});

					TweenMax.to(self.$el, 0.3, {
						opacity: 0,
						onComplete: function(){
							// self.$el.hide();
						}
					});
				}
			});
		}

	});		
	
});