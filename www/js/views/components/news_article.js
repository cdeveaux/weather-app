/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

News Article View
Author: Kitchen Prague 2015
*/

define([
	'app',
	'handlebars',
	'text!templates/hbs/components/news_article.hbs',
	'views/components/basic'
], function(App, Handlebars, Template) {

	App.Views.NewsArticleComponent = App.Views.BasicComponent.extend({

		events: {
		},

		className: 'col-sm-4 collection',

		template: Handlebars.compile(Template),

		mixins: [],

		onBeforeRender: function(){
	    },

	    onRender: function(){
	    	var self = this;

	    	self._super();
	    },

	    onBeforeDestroy: function(){
        },

	    setSizes: function(){
	    	var self = this;

    	}

	});		
	
});