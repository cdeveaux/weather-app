/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Product Preview Component

This component is being used to represent the specific product

Author: Kitchen Prague 2015
*/

define([
	'app',
	'handlebars',
	'text!templates/hbs/components/productPreview.hbs',
	'views/components/basic',
], function(App, Handlebars, Template) {

	App.Views.ProductPreviewComponent = App.Views.BasicComponent.extend({

		events: {
			'click .scrollDown' : 'scrollDown'
		},

		className: function(){
			return  this.model.className()
		},

		template: Handlebars.compile(Template),

		mixins: [],

		initialize: function(){
			var self = this;
			self._super();
		},

		onBeforeRender: function(){
	    },

	    onRender: function(){
	    	var self = this;

	    	self._super();
	    },

	    onBeforeDestroy: function(){
        },

	    setSizes: function(){
	    	var self = this;

	    	self._super();

	    	self.$el.css({
	    		height: App.height - App.topPadding - App.sidePadding
	    	});
	    }

	});		
	
});