define([
	'app',
	'handlebars',
	'text!templates/hbs/cityItem.hbs',
], function(App, Handlebars, Template) {

	App.Views.CityItem = CookBook.View.extend({
		template: Handlebars.compile(Template),
		tagName: 'li',

		className: function(){
			return 'cities__row js-temp-' +  getTempRange(this.model.get('currently').temperature);
		},
		
		events: {
			'click .js-cityDestroy' : 'removeItem'
		},

		initialize: function() {
			var self = this;

			_.bind(self.remove);
		},

	    onRender: function() {
	    	var self = this;
	    	
	    	App.trigger("item:rendered");
	    },

	    removeItem: function() {
	    	var self = this;

	    	self.model.destroy();
	    }

	});		
	
});