define([
	'app',
	'handlebars',
	'text!templates/hbs/layouts/cities.hbs',
	'views/items/city'
], function(App, Handlebars, Template) {

	App.Views.Cities = CookBook.View.extend({
		template: Handlebars.compile(Template),

		className: 'cities__page',

		regions: {
			citiesWrapper: '.citiesWrapper'
		},

		initialize: function(options) {
			var self = this;

			self.collection = options.data;
		},

	    onBeforeRender: function() {
	    	var self = this;
	    },

	    onRender: function() {
	    	var self = this;

			self.collectionView = new CookBook.CollectionView({
				collection : self.collection,
				tagName: 'ul',
				childView: App.Views.CityItem
			});

			self.citiesWrapper.show(self.collectionView);

	    }

	});
	
});