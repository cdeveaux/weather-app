define([
	'app',
	'handlebars',
	'text!templates/hbs/layouts/city.add.hbs'
], function(App, Handlebars, Template) {

	App.Views.CityAdd = CookBook.View.extend({
		template: Handlebars.compile(Template),

		className: 'cityAdd__page',

		regions: {
		},

		events: {
			'submit #addCity' : 'createCity'
		},

		createCity: function(e) {
			e.preventDefault();
			var self = this;

			// self.model.set('name', self.$('#city').val());
			self.model.create(self.$('#city').val()).then(function(d) {
				if (d && d.error) {
					alert(d.error);
				}
			});
		},

		initialize: function() {
			var self = this;

			// Init an empty model 
			self.model = new App.Models.City();

			self.model.on('reset', function(model) {
				// City has been created
				App.Router.navigate('/cities/' + model.get('name'), {
					trigger: true,
					replace: true
				});
			});

			_.bind(self.createCity, self);
		},

	    onBeforeRender: function() {
	    	var self = this;
	    },

	    onRender: function() {
	    	var self = this;
	    }

	});
	
});