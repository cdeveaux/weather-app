define([
	'app',
	'handlebars',
	'text!templates/hbs/layouts/city.hbs',
	'views/items/city'
], function(App, Handlebars, Template) {

	App.Views.City = CookBook.View.extend({
		template: Handlebars.compile(Template),
		
		regions: {
		    currentDay: '.city__currentDay',
		    daySelection: '.city__daySelection'
	  	},

	  	events: {
			'change #sliderHour' : 'updateHour'
	  	},

	  	className: function(){
			return 'city__page';
		},

	  	updateHour: function(e) {
			var self = this,
				target = e.currentTarget,
				hour = parseInt(target.value);

			self.$el
				.attr('class', 'city__page')
				.addClass('js-temp-' + getTempRange(_.find(self.currentDayData.hours, {hour: hour}).temperature));

			self.currentHour = hour;
			App.trigger('hour:change', hour);
		},

		initialize: function(options) {
			var self = this;

			self.model = options.data;
			self.days = self.model.get('days');
			self.currentDayData = self.days[0];
			self.currentHour = currentHour();

			self.listenTo(App, 'day:change', function(timestamp) {
				// Set the current day
				self.currentDayData = _.find(self.days, {time: timestamp});

				// First day, set the first hour as the current if we're out of range
				self.currentHour = Math.max(self.currentHour, self.currentDayData.hours[0].hour);
				
				self.$el
					.attr('class', 'city__page')
					.addClass('js-temp-' + getTempRange( _.find(self.currentDayData.hours, {hour: self.currentHour}).temperature ));
				
				self.render();
			});

			_.bind(self.updateHour, self);
		},

	    onBeforeRender: function() {
	    	var self = this;
	    },

	    onRender: function() {
	    	var self = this;

	    	// Set the current regions
	    	self.renderCurrent();
	    	self.renderNextDays();

	    	// ClassName is called before the initialize and doesn't have access to the model, set the class on render
	    	self.$el.addClass('js-temp-' +  getTempRange(_.find(self.currentDayData.hours, {hour: self.currentHour}).temperature));
	    },

	    renderCurrent: function() {
	    	var self = this,
                currentDay = App.getView('CityCurrent');

            self.currentView = new currentDay({
            	currentDay: self.currentDayData,
            	hour: self.currentHour,
            	name: self.model.get('name')
            });
            self.currentDay.show(self.currentView);
	    },

	    renderNextDays: function() {
	    	var self = this,
	    		daySelection = App.getView('CityDaySelection');

	    	self.daySelectionView = new daySelection({
	    		model: self.model,
	    		hour: self.currentHour
	    	})
	    	self.daySelection.show(self.daySelectionView);
	    },

	    serializeData: function() {
			var self = this;

			return _.extend({}, self.currentDayData, {
				minHour: self.currentDayData.hours[0].hour,
				currentHour: self.currentHour
			});
		}

	});
	
});