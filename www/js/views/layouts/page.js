/*
           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 
                                                                                                                                                                          

Basic Page Layout
Author: Kitchen Prague 2015
*/

define([
	'app',
	'handlebars',
	'text!templates/hbs/layouts/page.hbs'
], function(App, Handlebars, Template) {

	App.Views.Page = CookBook.View.extend({

		template: Handlebars.compile(Template),

		regions: {
		    componentsWrapper: ".components_wrapper"
	  	},

	  	className: function(){
			return  this.model.className()
		},

		initialize: function() {
			var self = this;

			// initialize the collection - do not use / change
			self.collection = new App.Collections.Components();

			self.listenTo(self.collection, "reset", function() {
				self.renderComponents();
			});

			self.fetchComponents();
		},

		fetchComponents: function() {
			var self = this,
				componentIds = self.model.getAttribute("components") || [];

			if (_.isEmpty(componentIds)) {
				self.collection.reset();
				return false;
			}

			self.collection.fetch({
                reset: true,
                remove: true,
                cache: true,
                query : {
					'id[in]' : componentIds.join(','),
					// 'locale' : App.options.locale
				},
                success: function(data){
                	App.trigger("page:rendered");
                }
	        });
		},

		renderComponents: function() {
			var self = this;

			if(!self.collection.models.length){
				return false;
			}

			self.collectionView = new CookBook.CollectionView({
				collection : self.collection,
				className: 'row'
			});

			self.componentsWrapper.show(self.collectionView);
		},

	    onBeforeRender: function() {
	    	var self = this;
	    },

	    onRender: function() {
	    }

	});
	
});