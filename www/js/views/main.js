define([
    'app',
    'handlebars',
    'text!templates/hbs/layout.hbs',
    // models
    'models/page',
    // plugins
    'helpers',
    'tweenmax'
], function(App, Handlebars, Template, Cocktail) {

    App.Views.Main = CookBook.View.extend({
        el: '#main',

        template: Handlebars.compile(Template),

        mixins: [],

        events: {
            'click .links': 'changeRoute'
        },

        regions: {
            footer: "#footer",
            page: "#page",
        },

        constructor: function() {
            var self = this;

            self._super();

            self.model = new App.Models.Page();

            // register events
            self.initEvents();
        },

        initialize: function() {
            var self = this;

            self.setSizes();

            App.firstInit = true;

            // register partial templates
            self.registerPartials();
        },

        initEvents: function() {
            var self = this;

            self.listenTo(self.model, "change", function(model) {
                App.trigger("after:page:change");
                App.trigger('page:ready');
            });

            App.on('page:ready', function() {
                self.renderLayout();
            })

            App.on('component:rendered', function() {
                self.setSizes();
            });

            App.on('page:rendered', function() {
                $("#loader").remove();
            });

            App.on('before:page:change', function() {});
        },

        changePage: function(data) {
            var self = this;

            // scroll to top
            TweenMax.to("body, html", 0.3, {
                scrollTop: 0
            });

            App.trigger("before:page:change");

            self.datas = data;
            App.trigger("after:page:change");
            App.trigger('page:ready');
        },

        changeRoute: function(e) {
            e.preventDefault();

            var self = this,
                $target = $(e.currentTarget),
                href = $target.attr("href"),
                isBlank = $target.attr('target') == '_blank',
                $navbar = $(".navbar-collapse");

            if (isBlank) {
                window.open(href, '_blank');
            } else {
                App.Router.navigate(href, true);
            }
        },

        renderLayout: function() {
            var self = this,
                // viewName = 'Cities' || self.model.getTemplate(),
                viewName = App.template,
                view = App.getView(viewName);
           
            // add new layout view
            self.layoutView = new view({
                data: self.datas
            });

            // show/render layout
            self.page.show(self.layoutView);
        },

        renderFooter: function() {
            var self = this,
                footer = App.getView("Footer");

            self.footerView = new footer();
            self.footer.show(self.footerView);
        },

        onRender: function() {
            var self = this;
        },

        setSizes: function(resize) {
            var self = this;

            App.width = getDocumentWidth();
            App.height = getDocumentHeight();

            
            App.trigger("page:setSizes");
        },

        registerPartials: function() {}
    });

});