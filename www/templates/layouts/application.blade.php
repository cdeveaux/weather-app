<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js oldie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js oldie lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js oldie lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js oldie ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js loading"> <!--<![endif]-->
<!--

           )       )  (                     )          )  
   (    ( /(    ( /(  )\ )  *   )   (    ( /(       ( /(  
 ( )\   )\())   )\())(()/(` )  /(   )\   )\()) (    )\()) 
 )((_) ((_)\  |((_)\  /(_))( )(_))(((_) ((_)\  )\  ((_)\  
((_)_ __ ((_) |_ ((_)(_)) (_(_()) )\___  _((_)((_)  _((_) 
 | _ )\ \ / / | |/ / |_ _||_   _|((/ __|| || || __|| \| | 
 | _ \ \ V /    ' <   | |   | |   | (__ | __ || _| | .` | 
 |___/  |_|    _|\_\ |___|  |_|    \___||_||_||___||_|\_| 


Developed by;
Filip Arneric, Velimir Matic & Christophe Deveaux                                                                                                                                                                              

-->
	<head>
		<meta charset="utf-8">
		<title>{{$meta_title or ''}}</title>
		<meta name="description" content="{{$meta_description or ''}}">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Kitchen">
		<meta name="csrf_token" content="{{ csrf_token() }}">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />

		<link rel="shortcut icon" href="/favicon.png">
		
	    <!-- Open graph tags -->
	    <meta property="og:title" content="{{$og_title or ''}}"/>
	    <meta property="og:description" content="{{$og_description or ''}}">
	    <meta property="og:type" content="{{$og_type or ''}}" />
	    <meta property="og:site_name" content="{{$og_site_name or ''}}"/>
	    <meta property="og:url" content="{{$og_url or ''}}" />
	    <meta property="og:image" content="{{$og_image or ''}}">
	    <meta property="og:locale" content="{{$og_locale or ''}}" />
		
		
		<!-- Generated CSS file -->
		<!-- <link href="{{ url() }}/css/main.min.css?v=1" rel="stylesheet"> -->
	    <link href="{{ url() }}/css/main.css?v=1" rel="stylesheet">
		
		<script>
	    	var absurl = '{{ url() }}',
	            locale = '{{ app::getLocale() }}',
	            ie8 = false,
	    		ie9 = false,
	    		ie = false;
		</script>

		<!--[if IE]>
			<script> var ie = true; </script>
			<script src='{{ url() }}/bower_components/html5shiv/dist/html5shiv.js'></script>
			<script src='{{ url() }}/bower_components/respond/src/respond.js'></script>
		<![endif]-->

		<!--[if lte IE 9]>
			<script> var ie9 = true; </script>
		<![endif]-->

		<!--[if lt IE 9]>
			<script> var ie8 = true; </script>
		<![endif]-->

	</head>
	<body>	

		<div id='loader'>
			<img src='{{ url() }}/img/logo.svg' class='center' height='100px' />	
		</div>

		<div id='main'></div>
		

		<!-- // <script data-main="{{ url() }}/js/app.min.js?v=10" src="{{ url() }}/js/require.js"></script> -->
	    <script data-main="{{ url() }}/js/load" src="{{ url() }}/bower_components/requirejs/require.js"></script>
		
	    <!-- Google Analytics -->
	 
	</body>
</html>